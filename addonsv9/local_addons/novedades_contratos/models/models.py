# -*- coding: utf-8 -*-

from openerp import models, fields, api


class Novedades(models.Model):
    _name = 'novedades'

    name = fields.Char(string='Nombre')
    code = fields.Char(string='Codigo')
    # tipo = fields.Selection((
    #     ('+', 'Subsidio'), ('-', 'Descuento')
    # ), string='Tipo', default='', required=True)


class NovedadMonto(models.Model):
    _name = 'novedades.monto'

    contract_id = fields.Many2one('hr.contract', string='Contrato')
    novedad_id = fields.Many2one('novedades', string='Concepto')
    monto = fields.Float()

    # @api.onchange('monto')
    # def _onchange_monto(self):
    #     print self.novedad_id.tipo, '  Tipo'
    #     if self.novedad_id.tipo == '-':
    #         self.monto = (-1 * self.monto)


class Contracts(models.Model):
    _inherit = 'hr.contract'

    novedades_ids = fields.One2many('novedades.monto', 'contract_id', 'Novedades')
