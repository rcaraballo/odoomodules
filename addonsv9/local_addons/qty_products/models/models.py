# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    total_products = fields.Float(string='Total de Productos',compute='_calc_total_of_products')

    @api.depends('invoice_line_ids')
    def _calc_total_of_products(self):
        for o in self:
            o.total_products = sum([line.quantity for line in o.invoice_line_ids])


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    total_products = fields.Float(compute='_calc_total_of_products')

    @api.depends('order_line')
    def _calc_total_of_products(self):
        for o in self:
            o.total_products = sum([line.product_uom_qty for line in o.order_line])


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    total_products = fields.Float(compute='_calc_total_of_products')

    @api.depends('pack_operation_product_ids')
    def _calc_total_of_products(self):
        for o in self:
            o.total_products = sum([line.product_qty for line in o.pack_operation_product_ids])

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    total_products = fields.Float(compute='_calc_total_of_products')

    @api.multi
    def _calc_total_of_products(self):
        for o in self:
            o.total_products = sum([l.product_qty for l in o.order_line])
