# -*- coding: utf-8 -*-
{
    'name': "qty_products",

    'summary': """
        Agrega en total de productos facturados en las factura.""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Ramon Caraballo",
    'website': "http://t.me/crbllo",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account','sale', 'stock', 'purchase'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
