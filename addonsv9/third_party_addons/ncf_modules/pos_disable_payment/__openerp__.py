{
    'name': "Disable payments in POS",
    'summary': "Control access to the POS payments",
    'version': '2.1.1',
    'author': 'IT-Projects LLC, Ivan Yelizariev',
    'license': 'LGPL-3',
    'category': 'Point Of Sale',
    'website': 'https://yelizariev.github.io',
    'images': ['images/pos_payment_access.png'],
    'depends': ['point_of_sale','base'],
    "price": 40.00,
    "currency": "EUR",    
    'data': [
        'views.xml',
        ],
    'installable': True,
}
