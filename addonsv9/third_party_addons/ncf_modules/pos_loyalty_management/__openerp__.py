# -*- coding: utf-8 -*-
#################################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#################################################################################
{
    'name':'POS: Loyalty & Rewards Program',
    'version':'1.0',
'summary':"Provide loyalty points on every purchase to your customers with some redemption benefits in Point Of Sale.",
# "license": 'OpenERP AGPL + Private Use',
    'category':'Point Of Sale',
    'author': 'Webkul Software Pvt. Ltd.',
    'description' : """

Loyalty & Rewards with POS
==========================
This module will Provide a loyalty program to your customers. Also Very Easy Steps For Point Redemption in Point Of Sale for your Customers.

NOTE: In Order To Assign Loyalty To Customer, You need to install Our POS Customer Module.

CHARACTERISTICS:
* Loyalty Rule Generation.
* Manage Different-2 Redemption Rules.
* Assign loyalty points to customers using Rules Version.
* redeemed points to give benefits of these loyality points to corresponding customers.
* Customer And Total Earned Points Will be Printed inside POS Receipt.

For any doubt or query email us at support@webkul.com or raise a Ticket on http://webkul.com/ticket/
    """,
    
    'depends': [
        'point_of_sale',
        'wk_wizard_messages',
    
        ],
    'website': 'http://www.webkul.com',
    'data': [
                'security/loyalty_management_security.xml',
                # 'rule_sequence.xml',
                'loyalty_management_view.xml',
                'security/ir.model.access.csv',
                'data/product.xml',
                'views/templates.xml',
            ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'auto_install': False,
    'active': False,
    "price": 69,
    "currency": 'EUR',
    "images":['static/description/Banner.png'],
}
