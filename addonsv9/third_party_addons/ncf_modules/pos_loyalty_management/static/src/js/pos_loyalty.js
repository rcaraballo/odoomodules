odoo.define('pos_loyalty.pos_loyalty', function (require) {
    "use strict";
    var screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');
    var PopUpWidget = require('point_of_sale.popups');
    var gui = require('point_of_sale.gui');
    var _t = require('web.core')._t;
    var Model = require('web.DataModel');
    var core = require('web.core');
    var QWeb = core.qweb;
    var utils = require('web.utils');
    var round_di = utils.round_decimals;
    var round_pr = utils.round_precision;

    models.load_fields('res.partner', 'wk_loyalty_points');
    models.load_fields('product.product', 'wk_point_for_loyalty');

    models.load_models([
        {
            model: 'loyalty.management',
            condition: function (self) {
                return true;
            },
            fields: ['loyalty_base', 'points', 'purchase', 'minimum_purchase'],
            domain: function (self) {
                return [['active', '=', true]];
            },
            loaded: function (self, result) {
                if (result.length) {
                    console.log("result");

                    console.log(result);
                    self.set('loyalty_base', result[0].loyalty_base);

                    self.set('loyalty_points', result[0].points);
                    self.set('loyalty_purchase', result[0].purchase);
                    self.set('loyalty_min_purchase', result[0].minimum_purchase);
                }

            },


        }], {'after': 'product.product'});


    var RedeemPopupWidget = PopUpWidget.extend({
        template: 'RedeemPopupWidget',
        show: function (opts) {
            var self = this;
            this._super(opts);

            this.$('#wk_redeem-now').click(function () {
                var currentOrder = self.pos.get_order();
                var discount_offer = currentOrder.get("discount_offer")
                var voucher_product_id = currentOrder.get("voucherProductId");
                // alert(voucher_product_id);
                var product = self.pos.db.get_product_by_id(voucher_product_id);
                var client = currentOrder.get('client');
                client.wk_loyalty_points = 0;

                currentOrder.add_product(product, {price: -discount_offer});
                currentOrder.set('redeemTaken', true);
                self.gui.close_popup();

            });
        },
    });
    gui.define_popup({name: 'redeemPopup', widget: RedeemPopupWidget});

    var CustomerRedeemWidget = screens.ActionButtonWidget.extend({
        template: 'CustomerRedeemWidget',
        button_click: function () {
            var order = this.pos.get_order();
            console.log(order);
            var current = this;
            // alert(order.get_total_with_tax());
            var client = order.get_client();
            if (client) {
                if (order.get_total_with_tax() > 0) {

                    if (!order.get('redeemTaken')) {
                        var loyalty_model = new Model('loyalty.management');
                        loyalty_model.call('get_voucher_product_id').fail(function (unused, event) {
                            //don't show error popup if it fails
                            event.preventDefault();
                            var msg = 'Failed to fetch Vouchser Product, Configure Loyalty Rules!!!'
                            console.error(msg);
                            alert(msg);
                        }).done(function (product_id) {
                            order.set('voucherProductId', product_id);
                            console.log("================")
                            console.log(client);
                            console.log("================")
                            loyalty_model.call('get_customer_loyality', [client.id])
                                .fail(function (unused, event) {
                                    //don't show error popup if it fails 
                                    event.preventDefault();
                                    var msg = 'Failed to fetch customer loyalty points';
                                    console.error(msg);
                                    alert(msg);
                                }).done(function (result) {
                                var tpoints = result.points;
                                var discount = result.discount;
                                if (discount != -1) {
                                    if (discount != 0) {
                                        if (tpoints > 0 && client.wk_loyalty_points > 0) {
                                            // dueTotal = currentOrder.getTotalTaxIncluded();
                                            var dueTotal = order.get_total_with_tax();
                                            var discount_offer = dueTotal > discount ? discount : dueTotal;
                                            discount_offer = round_di(parseFloat(discount_offer) || 0, current.pos.dp['Product Price']);
                                            order.set('discount_offer', discount_offer);
                                            order.set('total_points', tpoints);
                                            // var message = "Welcome "+client.name+"<br/><br/>Your Total Earned Points: "+tpoints+"<br/>The Total Discount:  "+discount_offer;
                                            current.gui.show_popup('redeemPopup', {
                                                'name': client.name,
                                                'points': tpoints,
                                                'discount': discount_offer,

                                            });
                                            // current.pos_widget.screen_selector.show_popup('pos-customer-redemption');
                                            // $("#pos-customer-redemption-summary").html(message);
                                        } else {
                                            alert(_t('Sorry You Cannot Redeem, Because Customer Has 0 Points!!!'));
                                        }
                                    } else {
                                        alert(_t('Sorry, You don`t have enough points to redeem !!!'));
                                    }

                                } else {
                                    alert(_t('Sorry No Redemption Calculation Found in Loyalty Rule. Please Add Redemption Rule First !!!'));
                                }
                            });
                        });
                    } else {
                        alert(_t('Sorry You Have Already Redeemed Fidelity points for this customer!!!'));
                    }
                }
                else {
                    alert(_t('Please add some Product(s) First !!!'));
                }
            }
            else {
                alert(_t('Please select Customer First !!!'));
            }
        },
    });

    screens.define_action_button({
        'name': 'loyalty',
        'widget': CustomerRedeemWidget,
        'condition': function () {
            // return this.pos.loyalty && this.pos.loyalty.rewards.length;
            return true;
        },
    });


    var _super = models.Order;
    models.Order = models.Order.extend({
        initialize: function (attributes, options) {

            _super.prototype.initialize.apply(this, arguments);
            this.set({
                redeemTaken: false,
                totalEarnedPoint: 0,
                voucherProductId: 0,
                discount_offer: 0,
                total_points: 0,
            });

        },
        remove_orderline: function (line) {
            var product_id = line.product.id;
            if (product_id == this.get('voucherProductId')) {
                this.get('client').wk_loyalty_points = this.get("total_points");
                this.set('redeemTaken', false);
                this.set('total_points', 0);
                this.set('discount_offer', 0);
            }
            _super.prototype.remove_orderline.apply(this, arguments);
        },
        // This is Updated function
        get_loyalty_points: function () {
            var orderLines = this.get_orderlines();
            var total_loyalty = 0;

            var loyalty_base = this.pos.get('loyalty_base');

            if (loyalty_base == 'category') {
                for (var i = 0; i < orderLines.length; i++) {
                    var line = orderLines[i];
                    if (line.product.wk_point_for_loyalty > 0) {
                        total_loyalty += round_pr(line.get_quantity() * line.product.wk_point_for_loyalty, 1);
                    }
                }
            }
            else {

                var currentOrder = this.pos.get_order();
                var tpointsvalue = currentOrder.get_total_with_tax();
                var points = this.pos.get('loyalty_points');
                var purchase = this.pos.get('loyalty_purchase');
                var minimum_purchase = this.pos.get('loyalty_min_purchase');
                var client = currentOrder.get_client();
                if (client && points && purchase) {
                    if (tpointsvalue >= minimum_purchase) {
                        total_loyalty = this.calculate_loyalty_points(tpointsvalue, purchase, points);
                        // console.log("points"+total_points);
                    }

                }
            }
            return total_loyalty;
        },
        calculate_loyalty_points: function (total, purchase, points) {

            return parseInt(total / purchase) * points;

        },
        validate: function () {
            var client = this.get('client');
            if (client) {

                client.wk_loyalty_points += this.get_loyalty_points();
            }
            _super.prototype.validate.apply(this, arguments);
        },
        export_for_printing: function () {
            var currentOrder = this.pos.get_order();
            var json = _super.prototype.export_for_printing.apply(this, arguments);
            json.wk_loyalty_points = this.get_loyalty_points();
            json.tpoints = currentOrder.get('totalEarnedPoint');
            json.redeemTaken = currentOrder.get('redeemTaken');
            // console.log('a');
            return json;
        },
        export_as_JSON: function () {
            var self = this;
            var currentOrder = self.pos.get_order();
            var json = _super.prototype.export_as_JSON.apply(this, arguments);
            // var wk_total_loyalty_points=currentOrder.get('totalEarnedPoint');
            // alert( this.get_loyalty_points());
            if (currentOrder != undefined) {
                json.wk_loyalty_points = this.get_loyalty_points();
                json.tpoints = currentOrder.get('totalEarnedPoint');
                json.redeemTaken = currentOrder.get('redeemTaken');
            }
            // console.log('b');
            return json;
        },
    });


    screens.OrderWidget.include({
        update_summary: function () {
            this._super();
            var self = this;
            var order = this.pos.get_order();
            // alert("orderline is testing");
            // var order = this.pos.get('selectedOrder');

            var $loypoints = $(this.el).find('.summary .loyalty-points');

            if (order.get_client()) {
                var points = order.get_loyalty_points();
                var points_total = order.get_client().wk_loyalty_points + points;
                var points_str = this.format_pr(points, 1);
                var total_str = this.format_pr(points_total, 1);
                if (points && points > 0) {

                    points_str = '+' + points_str;
                }
                $loypoints.replaceWith($(QWeb.render('LoyaltyPoints', {
                    widget: this,
                    totalpoints: total_str,
                    wonpoints: points_str
                })));
                $loypoints = $(this.el).find('.summary .loyalty-points');
                $loypoints.removeClass('oe_hidden');
            } else {
                $loypoints.empty();
                $loypoints.addClass('oe_hidden');
            }
            // _super_OrderWidget.prototype.update_summary.apply(this,arguments);
        },
    });
});

    
