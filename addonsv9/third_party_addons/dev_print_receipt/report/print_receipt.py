# -*- coding: utf-8 -*-
##############################################################################
#
#    @ Devintelle COunstlting Services Pvt Ltd.
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################
import time
from datetime import datetime
from dateutil import relativedelta
from openerp.osv import fields, osv
from openerp.report import report_sxw
from openerp.tools import amount_to_text_en


class print_receipt_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(print_receipt_report, self).__init__(cr, uid, name, context)
        self.localcontext.update({
        
            'convert': self.convert,
            'get_amount_lines': self.get_amount_lines,

        })
    
    def convert(self, amount, cur):
        return amount_to_text_en.amount_to_text(amount, 'en', cur);

    def get_amount_lines(self,o):
        amount=0.00
        if o.line_ids:
            for line in o.line_ids:
                amount += line.price_subtotal
        else:
            for line in o.line_ids:
                amount += line.price_subtotal
        return amount
            

class print_receipt(osv.AbstractModel):
    _name = 'report.dev_print_receipt.print_receipt_id'
    _inherit = 'report.abstract_report'
    _template = 'dev_print_receipt.print_receipt_id'
    _wrapped_report_class = print_receipt_report

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
