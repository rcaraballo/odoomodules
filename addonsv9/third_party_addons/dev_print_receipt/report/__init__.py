# -*- coding: utf-8 -*-
##############################################################################
#
#    @ Devintelle COunstlting Services Pvt Ltd.
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################
import print_receipt
import print_payment
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
