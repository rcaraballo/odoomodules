# -*- coding: utf-8 -*-
##############################################################################
#
#    @ Devintelle COunstlting Services Pvt Ltd.
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintellecs.com>).
#
##############################################################################
{
    'name': 'Print Receipt & Payments',
    'version': '1.0',
    'sequence': 1,
    'category': 'Account',
    'summary': 'Print Sale/Purchase Receipt, Customer/Supplier Payments in voucher',
    'description': """
         App will Print Sale/Purchase Receipt, Customer/Supplier Payments in voucher.
    """,
    'author': 'DevIntelle Consulting Service Pvt. Ltd',
    'website' : 'http://devintellecs.com/',
    'depends': ['account'],
    'data': [
        'print_receipt_view.xml',
        'report/print_receipt.xml',
        'report/print_payment_receipt.xml',
    ],
    'demo': [],
    'test': [],
    'css': [],
    'qweb': [],
    'js': [],
    'images': ['images/main_screenshot.png'],
    'installable': True,
    'application': True,
    'auto_install': False,
    'price':20.0,
    'currency':'EUR' 
    
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

